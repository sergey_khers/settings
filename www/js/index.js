/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        $.ajax({
            url:'http://kilograpp.com:8080/muhammad/api/hadiths',
            dataType: 'json',
            success: function(json){
                if(json.length) {
                    constructorDOM.fillCitations(json);
                } else {
                    constructorDOM.emptyCitation();
                }
            },
            error: function() {
                constructorDOM.errorCitations();
            }
        });
    }
};

app.initialize();
$(document).ready(
  function() {
    hello();
  }
);

function hello() {
  var a = [
    {
      id: 1,
      text: 'Приведут мужчину в День Суда и бросят в огонь, его кишки вывалятся из живота, и он закрутится в них, как крутится осел, вращающий жернов.\r\nТогда соберутся вокруг него жители огня и скажут: «О такой-то! Что с тобой?» Тот ответит: «Я приказывал совершать добро, но сам не делал его, и запрещал зло, а сам делал его',
      textSource: 'См.: Ан-Навави Я. Нузха аль-муттакын. Шарх рияд ас-салихин. Т. 1. С. 179, хадис № 200.',
      partialText:'Приведут мужчину в День Суда и бросят в огонь, его кишки вывалятся из живота, и он закрутится в них, как крутится осел, вращающий жернов. Тогда соберутся вокруг него жители огня и скажут: «О такой-то! Что с тобой?» Тот ответит: «Я приказывал совершать добро, но сам не делал его\".',
      likesCount: 15,
      creationDate: 1418342400
    }, {
      id: 2,
      text: 'Приведут мужчину в День Суда и бросят в огонь, его кишки вывалятся из живота, и он закрутится в них, как крутится осел, вращающий жернов.\r\nТогда соберутся вокруг него жители огня и скажут: «О такой-то! Что с тобой?» Тот ответит: «Я приказывал совершать добро, но сам не делал его, и запрещал зло, а сам делал его',
      textSource: 'См.: Ан-Навави Я. Нузха аль-муттакын. Шарх рияд ас-салихин. Т. 1. С. 179, хадис № 200.',
      partialText: 'Приведут мужчину в День Суда и бросят в огонь, его кишки вывалятся из живота, и он закрутится в них, как крутится осел, вращающий жернов. Тогда соберутся вокруг него жители огня и скажут: «О такой-то! Что с тобой?» Тот ответит: «Я приказывал совершать добро, но сам не делал его\".',
      likesCount: 21,
      creationDate: 1347235200
    }];
  constructorDOM.fillCitations(a)
}

//> oblect DOM constructor
var constructorDOM = {
    fillCitations: function(citations) {
        for(var i = 0; i < citations.length; i++) {
            var cDate = new Date(citations[i].creationDate);
            var cfDate = cDate.getDate() + '.' + (cDate.getMonth() + 1) + '.' + cDate.getYear();
            $('#deviceready')
              .append($('<div/>')
                .attr('id', 'citation' + i)
                .attr('class', 'citation_group')
                .append($('<div/>')
                  .attr('class', 'citation')
                  .append($('<div/>')
                    .attr('class', 'text')
                    .append($('<p/>')
                      .text(citations[i].partialText)
                      .append($('<img>')
                        .attr('src', 'img/citation_logo.png')
                        .attr('alt', 'citation logo')))
                    .append($('<div/>')
                      .attr('class', 'clear')))
                  .append($('<div/>')
                    .attr('class', 'citation_share')
                    .append($('<div/>')
                      .attr('class', 'amen on')
                      .append($('<div>')
                        .attr('class', 'likes')
                        .text(citations[i].likesCount))
                      .append($('<div/>')
                        .attr('class', 'cap')
                        .text('Аминь')))
                    .append($('<div/>')
                      .attr('class', 'share off')
                      .text('Поделиться'))
                    .append($('<div/>')
                      .attr('class', 'clear')))
                  .append($('<div/>')
                    .attr('class', 'creationDate')
                    .text('Хадис ' + cfDate))
                  .append($('<div/>')
                    .attr('class', 'citation_text')
                    .text(citations[i].text))
                  .append($('<div/>')
                    .attr('class', 'textSource')
                    .text(citations[i].textSource)
                    .append($('<div/>')
                      .attr('class', 'clear')))));
        }
    },
    emptyCitation: function() {
        $('#deviceready').append($('<div/>').text('-= no citation =-'))
    },
    errorCitations: function() {
        $('#deviceready').append($('<div/>').text('-= ERROR =-'))
    }
};
//< oblect DOM constructor